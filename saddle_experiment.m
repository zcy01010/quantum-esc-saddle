%% Escape from saddle points on quantum computers
% This script runs the 4 numerical experiments presented in the paper
% "Escape from Saddle Points on Quantum Computers"
% submitted to NeurIPS 2020 with paper ID 2851. Do not distribute.
% The computing environment is MATLAB 2019a. 

% In test 0 and 1, we use the finite difference method (FDM) and the 
% leapfrog scheme (LFS) to solve time-dependent Schrodinger equation. 
% The integrator 'leapfrogsolver' (available on MathWorks) was developed 
% by Mauger Fran�ois. 


% In test 2, we use a sampler ('randsphere') to generate random points 
% in an n-dim hypersphere. 
% It was developed by Roger Stafford, also availale in MathWorks.

% The initialization PDE parameters are the same as follows.


% set PDE grid
n = 512;
n1 = n+1;
a = 6/n1;
xaxis = -3+a:a:3-a;
yaxis = -3+a:a:3-a;
[X,Y]=meshgrid(xaxis,yaxis);

% set initial wave 
r = 0.5;
scalar = 1/(r*sqrt(2*pi));
init = @(x,y) scalar.*exp(-(x.^2+y.^2)/(4.*r.^2));
Phi0 = init(X,Y);
y0 = Phi0(:);

% build Laplacian matrix by FDM
L = lap2d(n)./a^2;

%% Test 0: Quantum Simulation for quadratic potentials
% In this test, we solve the Schrodinger equation 
% with potential field f(x,y) = -x^2/2+3y^2/2.
% The initial condition is set as in Prop A.1, with r = 0.5.
% The wave packets at t = 0, 0.5, 1 are illustrated in the figure.

% build Hamiltonian
func0 = @(x,y) -x.^2./2 + 3.*y.^2./2;
Z0 = func0(X,Y);
u0 = Z0(:);
U0 = spdiags(u0,0,n^2,n^2);
H0 = - r^2.*L./2 + U0./r^2;

% run simulation by LFS
tic
dt = 0.9*a^2;
tspan = [0,0.5,1];
y = leapfrogsolver(H0,y0,tspan,dt);
tEnd=toc;
fprintf('Test 0, running time = %d\n',tEnd);

%% plot
figure(1)
subplot(1,3,1)
surf(X,Y,abs(reshape(y(1,:),n,n)).^2+1,'FaceAlpha',0.85,'EdgeColor','none');
hold on
imagesc(xaxis,yaxis,abs(reshape(y(1,:),n,n)).^2);
set(gca,...
    'FontSize',12,'FontName','Times');
set(gcf, 'units','points','position',[0 10 1200 360]);
xticks([-2,-1,0,1,2]);
yticks([-2,-1,0,1,2]);
zticks([]);
xlabel('X')
ylabel('Y')
title('t = 0');

subplot(1,3,2)
surf(X,Y,abs(reshape(y(2,:),n,n)).^2+1,'FaceAlpha',0.85,'EdgeColor','none');
hold on
imagesc(xaxis,yaxis,abs(reshape(y(2,:),n,n)).^2);
set(gca,...
    'FontSize',12,'FontName','Times');
xticks([-2,-1,0,1,2]);
yticks([-2,-1,0,1,2]);
zticks([]);
xlabel('X')
ylabel('Y')
title('t = 0.5');

subplot(1,3,3)
surf(X,Y,abs(reshape(y(end,:),n,n)).^2+1,'FaceAlpha',0.85,'EdgeColor','none');
hold on
imagesc(xaxis,yaxis,abs(reshape(y(end,:),n,n)).^2);
set(gca,...
    'FontSize',12,'FontName','Times');
xticks([-2,-1,0,1,2]);
yticks([-2,-1,0,1,2]);
zticks([]);
xlabel('X')
ylabel('Y')
title('t = 1');
%% Test 1: Quantum Simulation for non-quadratic potentials

%% function 1 : f(x,y) = -x^2/2+y^2/2+x^4/12
% The coutour and surface plot of the wavepacket 
% at t = 1.5 is shown in the figure. 

% build Hamiltonian
func1 = @(x,y) -x.^2./2 + y.^2./2 + x.^4./12;
Z1 = func1(X,Y);
u1 = Z1(:);
U1 = spdiags(u1,0,n^2,n^2);
H1 = - r^2.*L./2 + U1./r^2;

% run simulation by LFS
tic
dt = 0.9*a^2;
y = leapfrogsolver(H1,y0,[0,1.5],dt);
tEnd=toc;
fprintf('Test 1, function 1, running time = %d\n',tEnd);

%% plot
figure(2)
subplot(1,2,1)
wavepacket = abs(reshape(y(2,:),n,n)).^2;
contour(X,Y,wavepacket,[0.05,0.15,0.35,0.45],'b-','LineWidth',2);
hold on
contour(X,Y,Z1, [-0.5,0,1,2,3],'ShowText','on');
plot([sqrt(3) -sqrt(3)],[0 0],'k*');
set(gca,...
    'TickDir','out','TickLength',[0.02 0.02],...
    'FontSize',12,'FontName','Times');
set(gcf, 'units','points','position',[0 0 1000 400]);
xticks([-2,-1,0,1,2]);
yticks([-2,-1,0,1,2]);
xlabel('X')
ylabel('Y')
legend('density','landscape','minimizer')
title('contour plot');

subplot(1,2,2)
surf(X,Y,wavepacket,...
    'FaceAlpha',0.85,'EdgeColor','none');
xlabel('X')
ylabel('Y')
colorbar 
title('surface plot');

%% function 2: f(x,y) = x^3-y^3-2xy+6

% build Hamiltonian
func2 = @(x,y) x.^3 - y.^3 - 2.*x.*y + 6;
Z2 = func2(X,Y);
u2 = Z2(:);
U2 = spdiags(u2,0,n^2,n^2);
H2 = - r^2.*L./2 + U2./r^2;

% run simulation by LFS
tic
dt = 0.9*a^2;
y = leapfrogsolver(H2,y0,[0,1,2,5],dt);
tEnd=toc;
fprintf('Test 1, function 2, running time = %d\n',tEnd);

%% plot
figure(3)
subplot(2,2,1)
imagesc(xaxis,yaxis,abs(reshape(y(1,:),n,n)).^2,'AlphaData',.75,[0 1]);
hold on
contour(X,Y,Z2,[-10,0,6,20,30],'w-','ShowText','on');
colorbar
set(gca,...
    'Ydir','normal',...
    'FontSize',12,'FontName','Times');
set(gcf, 'units','points','position',[0 0 700 600]);
xticks([-2,-1,0,1,2]);
yticks([-2,-1,0,1,2]);
xlabel('X')
ylabel('Y')
title('t = 0');

subplot(2,2,2)
imagesc(xaxis,yaxis,abs(reshape(y(2,:),n,n)).^2,'AlphaData',.75,[0 1]);
hold on
contour(X,Y,Z2,[-10,0,6,20,30],'w-','ShowText','on');
colorbar
set(gca,...
    'Ydir','normal',...
    'FontSize',12,'FontName','Times');
xticks([-2,-1,0,1,2]);
yticks([-2,-1,0,1,2]);
xlabel('X')
ylabel('Y')
title('t = 1');

subplot(2,2,3)
imagesc(xaxis,yaxis,abs(reshape(y(3,:),n,n)).^2,'AlphaData',.75,[0 1]);
hold on
contour(X,Y,Z2,[-10,0,6,20,30],'w-','ShowText','on');
colorbar
set(gca,...
    'Ydir','normal',...
    'FontSize',12,'FontName','Times');
xticks([-2,-1,0,1,2]);
yticks([-2,-1,0,1,2]);
xlabel('X')
ylabel('Y')
title('t = 2');

subplot(2,2,4)
imagesc(xaxis,yaxis,abs(reshape(y(4,:),n,n)).^2,'AlphaData',.75,[0 1]);
hold on
contour(X,Y,Z2,[-10,0,6,20,30],'w-','ShowText','on');
colorbar
set(gca,...
    'Ydir','normal',...
    'FontSize',12,'FontName','Times');
xticks([-2,-1,0,1,2]);
yticks([-2,-1,0,1,2]);
xlabel('X')
ylabel('Y')
title('t = 5');

%% Test 2: PGD & qPGD on non-quadratic function

%% Typical gradient descent paths
T = 20;
eta = .2;
y1 = [-0.01;-0.45];
y2 = [-0.35;0.07];
path1 = zeros(T,2);
path2 = zeros(T,2);
path1(1,:) = y1;
path2(1,:) = y2;

for k = 1:T-1
    z1 = y1 - eta.*grad(y1);
    z2 = y2 - eta.*grad(y2);
    y1 = z1;
    y2 = z2;
    
    path1(k+1,:) = z1';
    path2(k+1,:) = z2';
end
%%
eta = .05; % descent step size
r = 0.5; % radius of perturbation ball
M = 1000; % # of samples

outcome_pgd = zeros(1,M);
outcome_qpgd = zeros(1,M);
T1 = 50;
T2 = 10;

% q_PGD parameter
    mu = zeros(1,2);
    t_e = 1.5; % quantum evolution time
    eigval = [-1, 1];
    H = diag(eigval); 
    sig = arrayfun(@(s) var(t_e,s),eigval);
    Sig = diag(sig);
    
% sample
    seed_pgd = randsphere(M,2,r);
    seed_qpgd = mvnrnd(mu,Sig,M);

% PGD & qPGD
tic
    for t = 1:M
        % pgd for type 0
        x0 = seed_pgd(t,:)';
        xT = gd(x0,eta,T1);
        outcome_pgd(t) = f(xT);
        
        %q-pgd for type 0
        x0 = seed_qpgd(t,:)';
        xT = gd(x0,eta,T2);
        outcome_qpgd(t) = f(xT);
    end
tEnd=toc;
fprintf('Test 2, running time = %d\n',tEnd);

%% plot 
figure(4)
subplot(1,2,1)
plot(path1(:,1),path1(:,2),'-o');
hold on
plot(path2(:,1),path2(:,2),'-s');
plot([-0.01,-0.35],[-0.45,0.07],'kx','MarkerSize',20);
contour(X,Y,Z1, [-0.73,-0.7,-0.5,-0.3,-0.2,-0.1,0],'ShowText','on');
set(gca,...
    'FontSize',12,'FontName','Times');
set(gcf, 'units','points','position',[0 0 700 275]);
axis([-2 0.5 -0.5 0.5]);
xticks([-2,-1.5,-1,-0.5,0]);
yticks([-0.5,-0.25,0,0.25,0.5]);
legend('path 1','path 2');
xlabel('X')
ylabel('Y')
text(0,-0.47,'(-0.01,-0.45)');
text(-0.35,0.12,'(-0.35,0.07)');
title('Typical GD paths')

subplot(1,2,2)
binRange = -0.75:0.25:0;
hcx = histcounts(outcome_qpgd(:),[binRange Inf]);
hcy = histcounts(outcome_pgd(:),[binRange Inf]);
bar(binRange,[hcx;hcy]')
set(gca,...
    'XDir','reverse',...
    'FontSize',12,'FontName','Times');
xticks([-0.75 -0.5 -0.25 0]);
xticklabels({'(-0.75,-0.5]', '(-0.5,-0.25]','(-0.25,0]','>0'});
xlabel('Function value')
ylabel('Frequency')
legend('PGD+QSimulation','PGD','Location','northwest');
title('descent value')
%% Test 3: dimension dependence, type 1 Hessian

eta = 1; % descent step size
ep = 1e-2; % epsilon
r = 5e-1; % radius of perturbation ball
M = 1000; % # of samples

outcome_pgd1 = zeros(3,M);
outcome_qpgd1 = zeros(3,M);

tic
for p = 1:3
    % general parameters
    d = 10^p;
    T1 = 50*p^2+50; % # of classical iterations
    T2 = 30*p; % # of quantum iterations
    
    % q_PGD parameter
    mu = zeros(1,d);
    t_e = p; % quantum evolution time
    eigval1 = hd(ep,d); % type 1 Hessian
    H1 = diag(eigval1); 
    sig1 = arrayfun(@(s) var(t_e,s),eigval1);
    Sig1 = diag(sig1);
    
    % sample
    seed_pgd = randsphere(M,d,r);
    seed_qpgd = mvnrnd(mu,Sig1,M);
    
    % PGD & qPGD
    for t = 1:M
        % pgd for type 0
        x0 = seed_pgd(t,:)';
        xT = gdH(x0,eta,T1,H1);
        outcome_pgd1(p,t) = fH(xT,H1);
        
        %q-pgd for type 0
        x0 = seed_qpgd(t,:)';
        xT = gdH(x0,eta,T2,H1);
        outcome_qpgd1(p,t) = fH(xT,H1);
    end
end
tEnd=toc;
fprintf('Test 3, running time = %d\n',tEnd);

%% plot 
figure(5)
subplot(3,1,1)
binRange = -5e-3:1e-3:-5e-4;
hcx = histcounts(outcome_qpgd1(1,:),[binRange Inf]);
hcy = histcounts(outcome_pgd1(1,:),[binRange Inf]);
bar(binRange,[hcx;hcy]')
xticks([-5e-3,-4e-3,-3e-3,-2e-3,-1e-3]);
xticklabels({'(-5e-3,-4e-3]', '(-4e-3,-3e-3]','(-3e-3,-2e-3]','(-2e-3,-1e-3]','>-1e-3'});
legend('QSimulation','PGD');
set(gca,...
    'XDir','reverse',...
    'FontSize',12,'FontName','Times');
title('dim = 10')

subplot(3,1,2)
binRange = -1e-2:2e-3:-2e-3;
hcx = histcounts(outcome_qpgd1(2,:),[binRange Inf]);
hcy = histcounts(outcome_pgd1(2,:),[binRange Inf]);
bar(binRange,[hcx;hcy]')
xticks([-1e-2,-8e-3,-6e-3,-4e-3,-2e-3]);
xticklabels({'(-1e-2,-8e-3]', '(-8e-3,-6e-3]','(-6e-3,-4e-3]','(-4e-3,-2e-3]','>-2e-3'});
legend('QSimulation','PGD');
ylabel('frequency');
set(gca,...
    'XDir','reverse',...
    'FontSize',12,'FontName','Times');
title('dim = 100')

subplot(3,1,3)
binRange = -25e-2:5e-2:-5e-2;
hcx = histcounts(outcome_qpgd1(3,:),[binRange Inf]);
hcy = histcounts(outcome_pgd1(3,:),[binRange Inf]);
bar(binRange,[hcx;hcy]')
xticks([-0.25,-0.2,-0.15,-0.1,-0.05]);
xticklabels({'(-0.25,-0.2]', '(-0.2,-0.15]','(-0.15,-0.1]','(-0.05,-0.1]','>-0.1'});
legend('QSimulation','PGD');
xlabel('$f(x_T)$','Interpreter','latex');
set(gca,...
    'XDir','reverse',...
    'FontSize',12,'FontName','Times');
title('dim = 1000')


%%
function L = lap2d(n) % L on 2-d grid
e = ones(n,1);
B = spdiags([e -4*e e],-1:1,n,n);
A = spdiags([e 0*e e],-1:1,n,n);
I = speye(n);
L = kron(I,B) + kron(A,I);
end

function y = leapfrogsolver(H,y0,tspan,dt)
    % Mauger François (2020). Symplectic Leap Frog Scheme (https://www.mathworks.com/matlabcentral/fileexchange/38652-symplectic-leap-frog-scheme), MATLAB Central File Exchange. Retrieved May 28, 2020.
    N = length(y0);
    Q0 = y0;
    P0 = zeros(N,1);
    dV = @(t,Q) H*Q;
    dT = @(P) H*P;

    [~,Q1,P1] = sympleapfrog(dV, dT, tspan, Q0, P0, dt);
    y = Q1+1i.*P1;
end

function v = var(t,lambda)
    if lambda == 0
        v = 1 + t^2/4;
    elseif lambda > 0
        alpha = sqrt(lambda);
        r = 4*alpha^2;
        v = ((1+r)-(1-r)*cos(2*alpha*t))/(2*r);
    else 
        alpha = sqrt(-lambda);
        t = exp(2*alpha*t);
        v = ((1-t)^2+4*alpha^2*(1+t)^2)/(16*alpha^2*t);
    end
end

function x = gd(x0,eta,T)
for k = 1:T
    x = x0 - eta.*grad(x0);
    x0 = x;
end
end

function y = f(x)
y = -x(1).^2./2 + x(2).^2./2 + x(1).^4./12;
end

function v = grad(x)
v = [-x(1)+x(1).^3./3;x(2)];
end

function eigval = hd(ep,d) % build Hessian diagonal entries, type 1
    eigval = ones(1,d);
    eigval(1) = -ep;
end

function x = gdH(x0,eta,T,H)
for k = 1:T
    x = x0 - eta.*gradH(x0,H);
    x0 = x;
end
end

function y = fH(x,H)
y = .5* x' * H * x;
end

function v = gradH(x,H)
v = H * x;
end
