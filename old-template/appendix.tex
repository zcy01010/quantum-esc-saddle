\appendix
\newpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Auxiliary lemmas}
\subsection{Schr\"{o}dinger's equation with a quadratic potential}\label{append:QSimulation-append}
In this subsection, we prove several results that lay the foundation of the quantum algorithm described in \sec{QSimulation}.

\standQSimulation*

\begin{proof}
 Due to the well-posedness of the Schr\"odinger's equation, if we find a solution to the initial value problem \eqref{eqn:Schrodinger}, this solution is unique. We take the following ansatz
    \begin{equation} \label{eqn:ansatz}
        \Phi(t,x) = \left(\frac{1}{\pi}\right)^{1/4} \frac{1}{\sqrt{\delta(t)}}\exp(-i\theta(t)) \exp\left(\frac{-x^2}{2\delta(t)^2}\right),
    \end{equation}
    with $\theta(0) = 0$, $\delta(0) = \sqrt{2}$.

    In this Ansatz, the probability density $p_\lambda(t,x)$ , i.e., the modulus square of the wave function, is given by
    \begin{equation} \label{eqn:density}
        p_\lambda(t,x):= |\Psi(t,x)|^2 = \frac{1}{\sqrt{\pi}} \frac{1}{|\delta(t)|} \exp\Big(2 \operatorname{Im}(\theta(t))\Big) \exp\Big(-x^2 \operatorname{Re}(1/ y(t))\Big),
    \end{equation}
    where $y(t) = \delta^2(t)$.

    If the ansatz \eqref{eqn:ansatz} solves the Schr\"odinger equation, we will have the conservation of probability, i.e., $\|\Phi(t,x)\|^2 = 1$ for all $t \geq 0$; in other words, the $\int_\R p_\lambda(t,x) \d x = 1$ for all $t \geq 0$. It is now clear that \eqref{eqn:density} is the density of a Gaussian random variable with zero mean and variance
    \begin{equation} \label{eqn:variance0}
        \sigma^2(t;\lambda) = \frac{1}{2\operatorname{Re}(1/ y(t))}.
    \end{equation}
    Therefore, it is sufficient to compute $y(t)$ in order to obtain the distribution of the quantum particle at time $t\geq 0$. For simplicity, we will not compute the global phase $\theta(t)$ as it is not important in the the variance.

    Substituting the ansatz \eqref{eqn:ansatz} to \eqref{eqn:Schrodinger} with potential function $f(x) = \frac{\lambda}{2}x^2$, and introducing change of variables $y(t) = \delta^2(t)$, we attain the following system of ordinary differential equations

    \begin{equation} \label{eqn:ode}
    \begin{cases}
    y' +i\lambda y^2 - i = 0,\\
    \theta' = \frac{i}{4}\frac{y'}{y} + \frac{1}{2}\frac{1}{y},\\
    \theta(0) = 0, y(0) = 2.
    \end{cases}
    \end{equation}

    \paragraph{Case 1: $\lambda = 0$.} The system \eqref{eqn:ode} is linear with solutions
    \begin{equation}
    y(t) = 2+it.
    \end{equation}
    
    It follows that
    \begin{equation}
    \frac{1}{y(t)} = \frac{2}{4+t^2} - i \frac{t}{4+t^2},
    \end{equation}
    And by Equation \eqref{eqn:variance0}, the variance is
    \begin{equation}
    \sigma^2(t;0) = 1+\frac{t^2}{4}.
    \end{equation}

    \paragraph{Case 2: $\lambda \neq 0$.} The equation $y' + i\lambda y^2 - i = 0$ in \eqref{eqn:ode} is a Riccati equation. Using the standard change of variable $y = \frac{-i}{\lambda} \frac{u'}{u}$, we transfer the Riccati equation into a second-order linear equation
    \begin{equation} \label{eqn:ode2}
    u'' + \lambda u = 0.
    \end{equation}
    Clearly, the sign of $\lambda$ matters.

    \paragraph{Case 2.1: $\lambda > 0$.} Let $\alpha = \sqrt{\lambda}$, the solution to \eqref{eqn:ode2} is $u(t) = c_1 e^{i\alpha t} + c_2 e^{-i\alpha t}$ ($c_1, c_2$ are constants), and
    \begin{equation}
        y(t) = \frac{-i}{\lambda} \frac{u'}{u} = \frac{1}{\alpha}\frac{c_1 e^{i\alpha t} - c_2 e^{-i\alpha t} }{c_1 e^{i\alpha t} + c_2 e^{-i\alpha t}}.
    \end{equation}
    Provided the initial condition $y(0) = 2$, we choose $c_1 = 1$, $\beta := c_2 = (1-2\alpha)/(1+2\alpha)$, and it turns out that
    \begin{equation} \label{eqn:case2_1}
        y(t) = \frac{1}{\alpha}\Big(\frac{e^{2i\alpha t} - \beta }{e^{2i\alpha t} + \beta }\Big).
    \end{equation}
    By \eqref{eqn:variance0} and \eqref{eqn:case2_1}, we attain the variance when $\lambda > 0$.

    \paragraph{Case 2.2: $\lambda < 0$.} Let $\alpha = \sqrt{-\lambda} > 0$, similar as Case 2.1, we have
    \begin{equation} \label{eqn:case2_2}
        y(t) = \frac{i}{\alpha} \frac{e^{2\alpha t} - \beta}{e^{2\alpha t} + \beta},
    \end{equation}
    where $\beta = \frac{1+2i\alpha}{1-2i\alpha}$. And the variance $\sigma(t;\lambda)$ for $\lambda<0$ can be obtained from \eqref{eqn:variance0} and \eqref{eqn:case2_2}.
    \end{proof}

\begin{lemma}[$n$-dimensional evolution] \label{lem:standard_multidim_QSimulation}
Let $\mathcal{H}$ be a $n$-by-$n$ symmetric matrix with diagonalization $\mathcal{H} = U^T \Lambda U$, with $\Lambda = \diag(\lambda_1, ..., \lambda_n)$ and $U$ an orthogonal matrix. Suppose a quantum particle is in a $n$-dimensional potential field $f(\vect{x}) = \frac{1}{2}\vect{x}^T \mathcal{H}\vect{x}$ with initial state $\Phi(0,x)=(\frac{1}{2\pi})^{n/4} (\frac{1}{r})^{n/2}\exp(-\|\vect{x}\|^2/4r^2)$; in other words, the initial position of this quantum particle follows multivariate Gaussian distribution $\mathcal{N}(0,r^2 I)$. Then, at any time $t \ge 0$, the position of the quantum particle still follows multivariate Gaussian distribution $\mathcal{N}(0, \Sigma(t))$, with the covariance matrix
\begin{equation} \label{eqn:variance_n}
    \Sigma(t) = U^T \diag(\sigma^2(t;\lambda_1), ..., \sigma^2(t;\lambda_n))U.
\end{equation}
The function $\sigma(t;\lambda)$ is defined in \eqref{eqn:variance_standard}.
\end{lemma}

\begin{proof}
The proof follows the same idea in \lem{1_dim_standard_QSimulation}. We take the following ansatz
    \begin{equation} \label{eqn:ansatz_ndim}
        \Phi(t,\vect{x}) = \left(\frac{1}{\pi}\right)^{n/4} \left(\det \Delta(t)\right)^{-1/4}\exp(-i\theta(t)) \exp\left[-\frac{1}{2}\vect{x}^T \big(\Delta(t)\big)^{-1} \vect{x}\right],
    \end{equation}
    with $\theta(0) = 0$, $\Delta(0) = \sqrt{2}rI$, and $\Delta(t) = U^T \diag(\delta^2_1(t),...,\delta^2_n(t))U$.

The global phase parameter $\theta(t)$, together with the factor $\left(\frac{1}{\pi}\right)^{n/4} \left(\det \Delta(t)\right)^{-1/4}$, will contribute to a scalar factor in the probability density function such that the $L^2$-norm of the wave function \eqref{eqn:ansatz_ndim} will remain unit $1$. It is the matrix $\Delta(t)$ that controls the covariance matrix (see \eqref{eqn:variance1}). Regarding this, we do not delve into the derivation of $\theta(t)$ in this proof.

Substituting the ansatz \eqref{eqn:ansatz_ndim} to the Schr\"odinger equation \eqref{eqn:Schrodinger}, we have the following system of ordinary differential equations:

\begin{equation} \label{eqn:delta_ode}
    \frac{\d }{\d t}\left(\Delta(t)^{-1}\right) + i \Delta(t)^{-2} - i\mathcal{H} = 0,
\end{equation}
\begin{equation}
    \dot{\theta} = \frac{i}{4}\left(\det \Delta(t)\right)^{-1} \frac{\d }{\d t}\left(\Delta(t)\right)+\frac{1}{2}\Tr[\Delta(t)^{-1}].
\end{equation}
We immediately observe that Eq. \eqref{eqn:delta_ode} is a decoupled system
\begin{equation} \label{eqn:decouple}
    \frac{\d}{\d t}\left(\frac{1}{\delta_j(t)^2}\right) + i \frac{1}{(\delta_j(t))^4} - i \lambda_j = 0, \text{  for }j = 1,..., n.
\end{equation}
Again, introduce change of variables $y_j(t) = \delta^2_j(t)$, we have
\begin{equation} \label{eqn:decouple2}
    \dot{y}_j + i \lambda_j y^2-i=0, \text{  for }j = 1,..., n.
\end{equation}
They are precisely the same as the first equation in \eqref{eqn:ode}, thus the calculation of one-dimensional case in \lem{1_dim_standard_QSimulation} applies directly to \eqref{eqn:decouple2}.

Given the ansatz \eqref{eqn:ansatz_ndim}, it is clear that the probability density of the quantum particle in $\R^n$ is a $n-$dimensional Gaussian with mean $0$ and covariance matrix
\begin{equation} \label{eqn:variance1}
    \Sigma(t) = \left( 2\operatorname{Re} \Delta^{-1}(t)\right)^{-1} = U^T \left(\frac{1}{2\operatorname{Re}(1/y_1(t))},...,\frac{1}{2\operatorname{Re}(1/y_n(t))}\right) U.
\end{equation}
It follows from \eqref{eqn:variance0} and \eqref{eqn:variance_standard} that the covariance matrix is given as \eqref{eqn:variance_n}.
\end{proof}

\QSimulation*
\begin{proof}
Here, we only prove the one-dimensional case, as the $n$-dimensional case follows almost the same manner, together with a similar argument from the proof of \lem{standard_multidim_QSimulation}.
Let $\Phi(t,x)$ be the wave function as in \lem{1_dim_standard_QSimulation}, namely, it satisfies the standard Schr\"odinger's equation \eqref{eqn:Schrodinger}.
Define $\Psi(t,x) = \frac{1}{\sqrt{r}}\Phi(t,\frac{x}{r})$. Since $\|\Phi(t,\cdot)\|^2 = 1$ for all $ t \ge 0$, the factor $\frac{1}{\sqrt{r}}$ ensures the $L^2$-norm of $\Psi(t,x)$ is always 1.

We claim $\Psi(t,x)$ satisfies the modified Schr\"odinger's equation \eqref{eqn:Schrodinger_modified}. To do so, we substitute $\Psi(t,x)$ back to \eqref{eqn:Schrodinger_modified}. The left-hand-side is just $i \frac{\partial}{\partial_t} \frac{1}{\sqrt{r}}\Phi(t,x/r)$. The right-hand-side is
\begin{align}
    \Big[-\frac{r^2}{2}\nabla^{2}+\frac{1}{r^2}f(\vect{x})\Big]\Psi(t,x) = \frac{1}{\sqrt{r}}\Big[-\frac{1}{2}\nabla^{2}+\frac{1}{2}\left(\frac{\vect{x}}{r}\right)^T \mathcal{H} \left(\frac{\vect{x}}{r}\right) \Big] \Phi\left(t,\frac{x}{r}\right).
\end{align}
Since $\Phi(t,x)$ satisfies \eqref{eqn:Schrodinger}, it turns out that the left-hand-side equals to the right-hand-side. 

Since the variance of $\Phi(t,x)$ is $\sigma^2(t;\lambda)$, that of $\Psi(t,x) =\frac{1}{\sqrt{r}} \Phi(t,x/r)$ is nothing but just $r^2 \sigma^2(t;\lambda)$.
\end{proof}


Although the variance formula in \lem{1_dim_standard_QSimulation} is written in explicit form, it is a bit heavy to use in analysis. The following lemma uses elementary inequalities to give reasonable bounds.
    \begin{lemma}\label{lem:variance_estimate}
    When $\lambda > 0$,
    \begin{equation}
    \min \Big\{1, \frac{1}{2\alpha}\Big\} \le \sigma(t;\lambda) \le \max \Big\{1, \frac{1}{2\alpha}\Big\}.
    \end{equation}
    When $\lambda < 0$, let $\alpha = \sqrt{-\lambda}$,
    \begin{equation}
    \frac{1}{\sqrt{2}}\varphi(t;\alpha) \le \sigma(t;\lambda) \le \varphi(t;\alpha),
    \end{equation}
    with $\varphi(t;\alpha) = \frac{1}{2\alpha} \sinh(\alpha t) + \cosh(\alpha t)$.
    \end{lemma}
    \begin{proof}
    The first estimate follows from $\cos2\alpha t \in [0,1]$, while the second estimate follows from the inequality
    \[\frac{a+b}{2} \le \sqrt{\frac{a^2+b^2}{2}} \le \frac{a+b}{\sqrt{2}}.\]
    \end{proof}
\jql{Suggestions on how to merge Lemma 7 with 6: there are several ways to give a weaker lower bound. The current calculation made use of the fact $\frac{1}{\alpha}\sinh \alpha t \ge t$. In fact, this can be slightly improved because we have $\cosh x \ge 1+x^2/2$.
\[\frac{1}{\sqrt{2}}\varphi(t;\alpha) \ge \frac{1}{\sqrt{2}} (1+\alpha^2 t^2) + \frac{1}{2\sqrt{2}}t \ge \frac{1}{\sqrt{2}} + \frac{1}{2\sqrt{2}}t.\]
Of course, in Lemma 6, I use an inequality to get the lower bound. You can actually attain the linear lower bound without the inequality.}


%========================================================================

\subsection{Tail bound of escaping from saddle points by quantum simulation}\label{append:h-tail}
In this subsection, we prove:
\tyl{merge with Lemma 6}

\begin{lemma}\label{lem:h-tail}
\begin{align}
h(\mathscr{T}';-\sqrt{\rho\epsilon})\geq 1+\mathscr{T}'^{2},
\end{align}
where $\mathscr{T}'=\frac{\ell}{\sqrt{\rho\epsilon}}\cdot\iota$ and $h$ is defined in \eqn{h-t-lambda-negative}.
\end{lemma}

\begin{proof}
Recall \eqn{wave-variance} that $h(\mathscr{T}';-\sqrt{\rho\epsilon})$ equals to:
\begin{align}
h(\mathscr{T}';-\sqrt{\rho\epsilon})=\frac{((1+\sqrt{\rho\epsilon}r^{2})e^{2r(\rho\epsilon)^{-1/4}\mathscr{T}'}-(1-\sqrt{\rho\epsilon}r^{2}))^{2}+4\sqrt{\rho\epsilon}r^{2}}{4(1+\sqrt{\rho\epsilon}r^{2})\sqrt{\rho\epsilon}r^{2}e^{2r(\rho\epsilon)^{-1/4}\mathscr{T}'}}.
\end{align}
Let $\theta=r(\rho\epsilon)^{-1/4}$. The equation above can be converted to:
\begin{align}
h(\mathscr{T}';-\sqrt{\rho\epsilon})=\frac{((1+\theta^{2})e^{2\theta\mathscr{T}'}-(1-\theta^{2}))^{2}+4\theta^{2}}{4(1+\theta^{2})\theta^{2}e^{2\theta\mathscr{T}'}},
\end{align}
which can be further simplified as
\begin{align}
\frac{((1+\theta^{2})e^{2\theta\mathscr{T}'}-(1-\theta^{2}))^{2}+4\theta^{2}}{4(1+\theta^{2})\theta^{2}e^{2\theta\mathscr{T}'}}
&=\frac{(1+\theta^{2})^{2}e^{4\theta\mathscr{T}'}+(1-\theta^{2})^{2}-2(1-\theta^{4})e^{2\theta\mathscr{T}'}+4\theta^{2}}{4(1+\theta^{2})\theta^{2}e^{2\theta\mathscr{T}'}}\\
&=\frac{(1+\theta^{2})(e^{2\theta\mathscr{T}'}+e^{-2\theta\mathscr{T}'})-2(1-\theta^{2})}{4\theta^{2}}.
\end{align}
We denote $\mu:=2\theta\mathscr{T}'$. Note that $\mu>0$. By the Taylor expansion of $e^{\mu}$ with Lagrange form of remainder, there exists real numbers $\zeta,\xi\in (0,\mu)$ such that
\begin{align}
e^{\mu}&=1+\mu+\frac{\mu^{2}}{2}+\frac{\mu^{3}}{6}+\frac{e^{\zeta}}{24}\mu^{4}; \\
e^{-\mu}&=1-\mu+\frac{\mu^{2}}{2}-\frac{\mu^{3}}{6}+\frac{e^{-\xi}}{24}\mu^{4}.
\end{align}
Adding these two equations, we have
\begin{align}
e^{\mu}+e^{-\mu}\geq 2+\mu^{2}+\frac{\mu^{4}}{24}(e^{\zeta}+e^{-\xi})\geq 2+\mu^{2}+\frac{\mu^{4}}{24}(1+e^{-\mu})\geq 2+\mu^{2}.
\end{align}
In other words,
\begin{align}
e^{2\theta\mathscr{T}'}+e^{-2\theta\mathscr{T}'}\geq 2+(2\theta\mathscr{T}')^{2},
\end{align}
which results in
\begin{align}
\frac{(1+\theta^{2})(e^{2\theta\mathscr{T}'}+e^{-2\theta\mathscr{T}'})-2(1-\theta^{2})}{4\theta^{2}}
&\geq\frac{(1+\theta^{2})(2+4\theta^{2}\mathscr{T}'^{2})-2(1-\theta^{2})}{4\theta^{2}}\\
&\geq\frac{4\theta^{2}+4\theta^{2}\mathscr{T}'^{2}}{4\theta^{2}}\\
&=1+\mathscr{T}';
\end{align}
or equivalently,
\begin{align}
h(\mathscr{T}';-\sqrt{\rho\epsilon})\geq 1+\mathscr{T}'^{2}.
\end{align}

\end{proof}
