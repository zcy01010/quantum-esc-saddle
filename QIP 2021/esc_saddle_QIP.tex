\documentclass[11pt]{article}

\usepackage[margin=1in]{geometry}
\DeclareMathAlphabet{\mathbbold}{U}{bbold}{m}{n}

\usepackage{colonequals}
%\usepackage{thm-restate}
\usepackage{braket} %Provides \Bra{}, \Ket{}, etc.

% \usepackage[pagebackref]{hyperref}
\usepackage{hyperref}
\hypersetup{
    bookmarksnumbered=true, % If Acrobat bookmarks are requested, include section numbers
    unicode=false, % non-Latin characters in Acrobat�s bookmarks
    pdfstartview={FitH}, % fits the width of the page to the window
    pdftitle={Quantum Algorithms for Escaping from Saddle Points}, % title
    pdfauthor={Chenyi Zhang, Jiaqi Leng, Tongyang Li}, % author
    pdfsubject={}, % subject of the document
    pdfcreator={}, % creator of the document
    pdfproducer={}, % producer of the document
    pdfkeywords={}, % list of keywords
    pdfnewwindow=true, % links in new window
    colorlinks=true, % false: boxed links; true: colored links
    linkcolor=blue, % color of internal links
    citecolor=blue, % color of links to bibliography
    filecolor=blue, % color of file links
    urlcolor=blue % color of external links
}

\usepackage{times}
\usepackage[scaled=.90]{helvet}

\usepackage{titling}
\usepackage[compact]{titlesec}

\usepackage{amssymb,amsmath,amsthm,amsfonts}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage[numbers,comma,sort&compress]{natbib}
\usepackage{authblk}
\usepackage{graphicx}
\usepackage[font=small]{caption}
\usepackage[labelformat=simple]{subcaption}
\usepackage{float}
\usepackage[ruled,vlined,linesnumbered]{algorithm2e}
\usepackage{physics}
\usepackage{footnote}
\usepackage{xcolor}

\usepackage{float,multirow}

\makeatletter
\renewcommand{\paragraph}{%
  \@startsection{paragraph}{4}%
  {\z@}{2.25ex \@plus .5ex \@minus .3ex}{-1em}%
  {\normalfont\normalsize\bfseries}%
}
\makeatother

%-----------------------------------------------------------------------------%
% Theorem-like environments and related macros:
%-----------------------------------------------------------------------------%

\newtheoremstyle{newstyle}
{0.5ex} %Aboveskip
{0.4ex} %Below skip
{\itshape} %Body font e.g.\mdseries,\bfseries,\scshape,\itshape
{} %Indent
{\bfseries} %Head font e.g.\bfseries,\scshape,\itshape
{.} %Punctuation afer theorem header
{ } %Space after theorem header
{} %Heading

\theoremstyle{newstyle}

\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{example}{Example}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}

%\numberwithin{equation}{section}
%\numberwithin{theorem}{section}
%\numberwithin{definition}{section}
%\numberwithin{lemma}{section}
%\numberwithin{proposition}{section}
%\numberwithin{example}{section}
%\numberwithin{corollary}{section}
%\numberwithin{remark}{section}

\usepackage{thm-restate}

\newcommand{\specialcell}[2][c]{%
  \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}}

\newcommand{\eqn}[1]{(\ref{eqn:#1})}
\newcommand{\eq}[1]{(\ref{eq:#1})}
\newcommand{\rem}[1]{\hyperref[rem:#1]{Remark~\ref*{rem:#1}}}
\newcommand{\thm}[1]{\hyperref[thm:#1]{Theorem~\ref*{thm:#1}}}
\newcommand{\cor}[1]{\hyperref[cor:#1]{Corollary~\ref*{cor:#1}}}
\newcommand{\defn}[1]{\hyperref[defn:#1]{Definition~\ref*{defn:#1}}}
\newcommand{\lem}[1]{\hyperref[lem:#1]{Lemma~\ref*{lem:#1}}}
\newcommand{\prop}[1]{\hyperref[prop:#1]{Proposition~\ref*{prop:#1}}}
\newcommand{\fig}[1]{\hyperref[fig:#1]{Figure~\ref*{fig:#1}}}
\newcommand{\tab}[1]{\hyperref[tab:#1]{Table~\ref*{tab:#1}}}
\newcommand{\algo}[1]{\hyperref[algo:#1]{Algorithm~\ref*{algo:#1}}}
\renewcommand{\sec}[1]{\hyperref[sec:#1]{Section~\ref*{sec:#1}}}
\newcommand{\append}[1]{\hyperref[append:#1]{Appendix~\ref*{append:#1}}}
\newcommand{\fac}[1]{\hyperref[fac:#1]{Fact~\ref*{fac:#1}}}
\newcommand{\lin}[1]{\hyperref[lin:#1]{Line~\ref*{lin:#1}}}
\newcommand{\fnote}[1]{\hyperref[fnote:#1]{Footnote~\ref*{fnote:#1}}}
\newcommand{\itm}[1]{\ref{itm:#1}}

\newcommand\vecc[1]{\mathbf{#1}}

\newcommand{\algname}[1]{\textup{\texttt{#1}}}
\SetKwInput{KwInput}{Input}
\SetKwInput{KwOutput}{Output}

\def\>{\rangle}
\def\<{\langle}
\def\trans{^{\top}}

\newcommand{\vect}[1]{\ensuremath{\mathbf{#1}}}
\newcommand{\x}{\ensuremath{\mathbf{x}}}

\newcommand{\N}{\mathbb{N}}
\newcommand{\R}{\mathbb{R}}
\renewcommand{\P}{\mathbb{P}}

\let\var\relax

\DeclareMathOperator{\poly}{poly}
\DeclareMathOperator{\diag}{diag}

\newcommand{\tnb}{\tilde{\nabla}}
\newcommand{\ugrad}{\mathscr{G}}
\newcommand{\ufun}{\mathscr{F}}
\newcommand{\uspace}{\mathscr{S}}
\newcommand{\utime}{\mathscr{T}}
\newcommand{\logt}{\iota}

\renewcommand{\d}{\mathrm{d}}
\def\Tr{\operatorname{Tr}}
\newcommand{\range}[1]{[#1]}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setlength{\droptitle}{-21mm}

\title{Quantum Algorithms for Escaping from Saddle Points}

\usepackage{authblk}
\author[1]{Chenyi Zhang}
\author[2,3]{Jiaqi Leng}
\author[3,4]{Tongyang Li}

\affil[1]{Institute for Interdisciplinary Information Sciences, Tsinghua University}

\affil[2]{Department of Mathematics, University of Maryland}

\affil[3]{QuICS and UMIACS, University of Maryland}

\affil[4]{Center for Theoretical Physics, Massachusetts Institute of Technology}

%\author{Joran van Apeldoorn\quad Shouvanik Chakrabarti\quad Andrew M. Childs\quad Andr\'as Gily\'en\\ Sander Gribling\qquad Tongyang Li\qquad Ronald de Wolf\qquad Xiaodi Wu}

\begin{document}

\date{}
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\renewcommand{\abstractname}{\vspace*{-18mm}}

\begin{abstract}
We initiate the study of quantum algorithms for escaping from saddle points with provable guarantee. Given a function $f\colon\R^{n}\to\R$, our quantum algorithm outputs an $\epsilon$-approximate local minimum using $\tilde{O}(\log^{2} n/\epsilon^{1.75})$\footnote{The $\tilde{O}$ notation omits poly-logarithmic terms, i.e., $\tilde{O}(g)=O(g\poly(\log g))$.} queries to the quantum evaluation oracle (i.e., the zeroth-order oracle). Compared to the classical state-of-the-art algorithm by Jin et al.~with $\tilde{O}(\log^{6} n/\epsilon^{1.75})$ queries to the gradient oracle (i.e., the first-order oracle), our quantum algorithm is polynomially better in terms of $n$ and matches its complexity in terms of $1/\epsilon$. Our quantum algorithm is built upon two techniques: First, we replace the classical perturbations in gradient descent methods by simulating quantum wave equations, which constitutes the polynomial speedup in $n$ for escaping from saddle points. Second, we show how to use a quantum gradient computation algorithm due to Jordan to replace the classical gradient queries in nonconvex optimization by quantum evaluation queries with the same complexity, extending the same result from convex optimization due to van Apeldoorn et al. and Chakrabarti et al. Finally, we also perform numerical experiments that support our quantum speedup.
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\paragraph{Motivations.}
Nonconvex optimization has been a central research topic in optimization theory in the past decade, mainly because the loss functions in many machine learning models (including neural networks) are typically nonconvex. However, finding the global optima of a nonconvex function is NP-hard in general. Instead, many theoretical works focus on finding local optima, since there are landscape results suggesting that local optima are nearly as good as the global optima for many learning problems (see e.g.~\cite{ge2015escaping,ge2017optimization,ge2018learning,bhojanapalli2016global,ge2016matrix,hardt2018gradient}). On the other hand, it is known that saddle points (and local maxima) can give highly suboptimal solutions in many problems; see e.g.~\cite{jain2017global,sun2018geometric}. Furthermore, saddle points are ubiquitous in high-dimensional nonconvex optimization problems~\cite{dauphin2014identifying,bray2007statistics,fyodorov2007replica}.

Therefore, one of the most important problems in nonconvex optimization theory is to \emph{escape from saddle points}. Suppose we have a twice-differentiable function $f\colon\R^{n}\to\R$ such that
\begin{itemize}
\item $f$ is $\ell$-smooth: $\|\nabla f(\x_{1})-\nabla f(\x_{2})\|\leq\ell\|\x_{1}-\x_{2}\|\quad\forall\,\x_{1},\x_{2}\in\R^{n}$,
\item $f$ is $\rho$-Hessian Lipschitz: $\|\nabla^{2} f(\x_{1})-\nabla^{2} f(\x_{2})\|\leq\rho\|\x_{1}-\x_{2}\|\quad\forall\,\x_{1},\x_{2}\in\R^{n}$;
\end{itemize}
the goal is to find an $\epsilon$-approximate local minimum $\x_{\epsilon}$ such that\footnote{In general, we can ask for an $(\epsilon_{1},\epsilon_{2})$-approximate local minimum $\x$ such that $\|\nabla f(\x)\|\leq\epsilon_{1}$ and $\lambda_{\min}(\nabla^{2}f(\x))\geq-\epsilon_{2}$. The scaling in \eqn{eps-approx-local-min} was first adopted by~\cite{nesterov2006cubic} and has been taken as a standard by subsequent works (e.g.~\cite{jin2019stochastic,jin2017escape,jin2018accelerated,xu2017neon,xu2018first,carmon2018accelerated,agarwal2017finding,tripuraneni2018stochastic,fang2019sharp}).}
\begin{align}\label{eqn:eps-approx-local-min}
\|\nabla f(\x_{\epsilon})\|\leq\epsilon,\quad\lambda_{\min}(\nabla^{2}f(\x_{\epsilon}))\geq-\sqrt{\rho\epsilon}.
\end{align}
There have been two main considerations on designing algorithms for escaping from saddle points. First, algorithms with good performance in practice are typically dimension-free or almost dimension-free (i.e., having $\poly(\log n)$ dependence), especially considering that most machine learning models in the real world have enormous dimensions. Second, practical algorithms prefer simple oracle access to the nonconvex function. If we are given a Hessian oracle of $f$, which takes $\x$ as the input and outputs $\nabla^{2} f(\x)$, we can find an $\epsilon$-approximate local minimum by second-order methods; for instance, Ref.~\cite{nesterov2006cubic} takes $O(1/\epsilon^{1.5})$ queries. However, because the Hessian is an $n\times n$ matrix, its construction takes $\Omega(n^{2})$ cost in general. Therefore, it has become a notable interest to escape from saddle points using simpler oracles.

A seminal work along this line was by Ge et al.~\cite{ge2015escaping}, which can find an $\epsilon$-approximate local minimum satisfying \eqn{eps-approx-local-min} only using the first-order oracle, i.e., gradients. Although this paper has a $\poly(n)$ dependence in the query complexity of the oracle, the follow-up work~\cite{jin2017escape} can make it almost dimension-free with complexity $\tilde{O}(\log^{4} n/\epsilon^{2})$, and the state-of-the-art result takes $\tilde{O}(\log^{6} n/\epsilon^{1.75})$ queries~\cite{jin2018accelerated}. However, these results suffer from a significant overhead in terms of $\log n$, and it has been an open question to keep both the merits of using only the first-order oracle as well as being close to dimension-free~\cite{jordan2017talk}.

In this paper, we explore quantum algorithms for escaping from saddle points. This is a mutual generalization of both classical and quantum algorithms for optimization:
\begin{itemize}[leftmargin=*]
\item For quantum computing, the vast majority of previous quantum optimization algorithms had been devoted to convex optimization with the focuses on semidefinite programs~\cite{brandao2016quantum,vanApeldoorn2017quantum,brandao2017SDP,vanApeldoorn2018SDP,kerenidis2018interior} and general convex optimization~\cite{vanApeldoorn2020optimization,chakrabarti2020optimization}; these results have at least a $\sqrt{n}$ dependence in their complexities, and their quantum algorithms are far from dimension-free methods. Up to now, little is known about quantum algorithms for nonconvex optimization.

    However, there are inspirations that quantum speedups in nonconvex scenarios can potentially be more significant than convex scenarios. In particular, \emph{quantum tunneling} is a phenomenon in quantum mechanics where the wave function of a quantum particle can tunnel through a potential barrier and appear on the other side with significant probability. This very much resembles escaping from poor landscapes in nonconvex optimization. Moreover, quantum algorithms motivated by quantum tunneling will be essentially different from those motivated by the Grover search~\cite{grover1996fast}, and will demonstrate significant novelty if the quantum speedup compared to the classical counterparts is more than quadratic.

\item For classical optimization theory, since many classical optimization methods are physics-motivated, including Nesterov's momentum-based methods~\cite{nesterov1983method}, stochastic gradient Langevin dynamics~\cite{zhang2017hitting} or Hamiltonian Monte Carlo~\cite{gao2018global}, etc., the elevation from classical mechanics to quantum mechanics can potentially bring more observations on designing fast \emph{quantum-inspired classical algorithms}. In fact, quantum-inspired classical machine learning algorithms have been an emerging topic in theoretical computer science~\cite{tang2019quantum,tang2018quantum2,chia2020SDP,gilyen2018quantum,chia2018quantum,chia2019framework}, and it is worthwhile to explore relevant classical algorithms for optimization.
\end{itemize}

%=============================================================
\paragraph{Contributions.}
Our main contribution is a quantum algorithm that finds an $\epsilon$-approximate local minimum with polynomial quantum speedup in $n$ compared to the classical state-of-the-art result~\cite{jin2018accelerated} using the gradient oracle (the first-order oracle). Furthermore, our quantum algorithm only takes queries to the \emph{quantum evaluation oracle} (the zeroth-order oracle), which is defined as a unitary map $U_{f}$ on $\R^{n}\otimes\R$ such that
\begin{align}\label{eqn:quantum-evaluation}
U_{f}|\x\>|\vect{0}\>=|\x\>|f(\x)\>\quad\ \forall \x\in\R^{n}.
\end{align}
Note that if the classical evaluation oracle can be implemented by explicit arithmetic circuits, the quantum evaluation oracle in \eqn{quantum-evaluation} can be implemented by quantum arithmetic circuits of the same size up to poly-logarithmic factors. As a result, it has been the standard assumption in previous literature on quantum algorithms for convex optimization~\cite{vanApeldoorn2020optimization,chakrabarti2020optimization}, and we subsequently adopt it here for nonconvex optimization.

\begin{theorem}[Main result, informal]\label{thm:main-intro}
There is a quantum algorithm that finds an $\epsilon$-approximate local minimum satisfying \eqn{eps-approx-local-min}, using $\tilde{O}(\log^{2} n/\epsilon^{1.75})$ queries to the quantum evaluation oracle~\eqn{quantum-evaluation}.
\end{theorem}

\begin{table}[htbp]
\centering
\begin{tabular}{ccc}
\hline
Reference & Queries & Oracle \\ \hline
\cite{nesterov2006cubic,curtis2017trust} & $O(1/\epsilon^{1.5})$ & Hessian \\ \hline
\cite{agarwal2017finding,carmon2018accelerated} & $\tilde{O}(\log n/\epsilon^{1.75})$ & Hessian-vector product \\ \hline
\cite{jin2017escape,jin2019stochastic} & $\tilde{O}(\log^{4} n/\epsilon^{2})$ & Gradient \\ \hline
\cite{jin2018accelerated} & $\tilde{O}(\log^{6} n/\epsilon^{1.75})$ & Gradient \\ \hline\hline
\textbf{this work} & $\tilde{O}(\log^{2} n/\epsilon^{1.75})$ & Quantum evaluation \\
\hline
\end{tabular}
\caption{A summary of the state-of-the-art works on finding an approximate local minimum using different oracles. The query complexities are highlighted in terms of the dimension $n$ and the error $\epsilon$.}
\label{tab:main}
\end{table}

Technically, our work is inspired by both the perturbed gradient descent (PGD) algorithm in~\cite{jin2017escape,jin2019stochastic} and the perturbed accelerated gradient descent (PAGD) algorithm in~\cite{jin2018accelerated}. To be more specific, PGD applies gradient descent iteratively until it reaches a point with small gradient. It can potentially be a saddle point, so PGD applies uniform perturbation in a small ball centered at that point and then continues the GD iterations. It can be shown that with an appropriate choice of the radius, PGD can shake the point off from the saddle and converge to a local minimum with high probability. The PAGD in~\cite{jin2018accelerated} adopts the similar perturbation idea, but the GD is replaced by Nesterov's AGD~\cite{nesterov1983method}.

Our quantum algorithm is built upon PGD and PAGD and shares their simplicity of being single-loop, but we propose two main modifications. On the one hand, for the perturbation steps for escaping from saddle points, we replace the uniform perturbation by evolving a quantum wave function governed by the Schr\"{o}dinger equation and using the measurement outcome as the perturbed result. Intuitively, the Schr\"odinger equation is able to screen the local geometry of a saddle point through wave interference, which results in a phenomenon that the wave packet disperses rapidly along the directions with significant function value decrease. Specifically, quantum mechanics finds the negative curvature directions more efficiently than the classical counterpart: for a constant $\epsilon$, the classical PGD takes $O(\log n)$ steps to decrease the function value by $\Omega(1/\log^3 n)$ with high probability, and the PAGD takes $O(\log n)$ steps to decrease the function value by $\Omega(1/\log^5 n)$ with high probability. Quantumly, the simulation of the Schr\"{o}dinger equation for time $t$ takes $\tilde{O}(t\log n)$ evaluation queries, but simulation for time $t=O(\log n)$ and $O(\log n)$ subsequent GD iterations suffice to decrease the function value by $\Omega(1)$ with high probability. This is summarized as~\tab{comparison} below. In addition, our quantum algorithm is \emph{classical-quantum hybrid}: the transition between consecutive iterations is still classical, while the only quantum computing part happens in each iteration for replacing the classical uniform perturbation.

\begin{table}[htbp]
\centering
\begin{tabular}{ccccc}
\hline
& Perturbation & \specialcell{\# iterations/\\simulation time} & \specialcell{Function\\decrease} & \specialcell{Queries in\\each iteration/unit time} \\ \hline
Classical & Uniform in ball & $O(\log n)$ & $\Omega(1/\log^{5} n)$ & 1 \\ \hline
Quantum & Quantum simulation & $O(\log n)$ & $\Omega(1)$ & $\tilde{O}(\log n)$ \\
\hline
\end{tabular}
\caption{A detailed comparison between our result and the classical state-of-the-art result~\cite{jin2018accelerated}, assuming $\epsilon=\Theta(1)$.}
\label{tab:comparison}
\end{table}

On the other hand, for the gradient descent steps, we replace them by a quantum algorithm for computing gradients using also quantum evaluation queries. The idea was initiated by Jordan~\cite{jordan2005fast} who computed the gradient at a point by applying the quantum Fourier transform on a mesh near the point. Prior arts have shown how to apply Jordan's algorithm for convex optimization~\cite{chakrabarti2020optimization,vanApeldoorn2020optimization}, and we conduct a detailed analysis showing how in nonconvex optimization we can replace classical gradient queries by the same number of quantum evaluation queries. Technically, we essentially show the robustness of escaping from saddle points by PGD, which may be of independent interest.

Finally, we perform numerical experiments that support our quantum speedup. Specifically, we observe the dispersion of quantum wave packets along the negative curvature direction in various landscapes. In a comparative study, our PGD with quantum simulation outperforms the classical PGD with a higher probability of escaping from saddle points and fewer iteration steps. We also compare the dimension dependence of classical and quantum algorithms in a model question with dimensions varying from 10 to 1000, and our quantum algorithm achieves a better dimension scaling overall.

%\vspace{-3mm}
%\paragraph{Related works.}
%Escaping from saddle points by gradients was initiated by Ge et al.~\cite{ge2015escaping} with complexity $O(\poly(n/\epsilon))$. The follow-up work~\cite{levy2016power} improved it to $O(n^{3}\poly(1/\epsilon))$, but it is still polynomial in dimension $n$. The breakthrough result by Jin et al.~\cite{jin2017escape,jin2019stochastic} achieves iteration complexity $\tilde{O}(\log^{4} n/\epsilon^{2})$ which is poly-logarithmic in $n$. The best-known result has complexity $\tilde{O}(\log^{6} n/\epsilon^{1.75})$ by~\cite{jin2018accelerated} (the same result in terms of $\epsilon$ was independently obtained by~\cite{allen2018neon2,xu2017neon}). Besides the gradient oracle, escaping from saddle points can also be achieved using the Hessian-vector product oracle with $\tilde{O}(\log n/\epsilon^{1.75})$ queries~\cite{agarwal2017finding,carmon2018accelerated}.
%
%There has also been a rich literature on stochastic optimization algorithms for finding second-order stationary points only using the first-order oracle. The seminal work~\cite{ge2015escaping} showed that noisy stochastic gradient descent (SGD) finds second-order stationary points in $O(\poly(n)/\epsilon^{4})$ iterations. This is later improved to $\tilde{O}(\poly(\log n)/\epsilon^{3.5})$~\cite{allen2018natasha2,tripuraneni2018stochastic,fang2019sharp,allen2018neon2,xu2018first}, and the current state-of-the-art iteration complexity of stochastic algorithms is $\tilde{O}(\poly(\log n)/\epsilon^{3})$ due to~\cite{fang2018spider,zhou2019stochastic}.
%
%Quantum algorithms for nonconvex optimization with provable guarantee is a widely open topic. As far as we know, the only work along this direction is~\cite{zhang2019quantum}, which gives a quantum algorithm for finding the negative curvature of a point in time $\tilde{O}(\poly(r,1/\epsilon))$, where $r$ is the rank of the Hessian at that point. However, the algorithm has a few drawbacks: 1) The cost is expensive when $r=\Theta(n)$; 2) It relies on a quantum data structure~\cite{kerenidis2016recommendation} which can actually be dequantized to classical algorithms with comparable cost~\cite{tang2019quantum,tang2018quantum2,chia2019framework}; 3) It can only apply negative curvature finding for one iteration. In all, it is unclear whether this quantum algorithm achieves speedup for escaping from saddle points.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\clearpage
\bibliographystyle{myhamsplain}
\bibliography{EscapeSaddle}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
